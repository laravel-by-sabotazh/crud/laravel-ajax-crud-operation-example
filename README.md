## Tutorial

From [here](https://www.itsolutionstuff.com/post/laravel-9-ajax-crud-tutorial-exampleexample.html).

## License

Licensed under the [MIT license](https://opensource.org/licenses/MIT).
